require "application_system_test_case"

class HomePagesTest < ApplicationSystemTestCase
  test "visiting the index" do
    Post.create!(title: 'my title')

    visit 'home_page/index'

    Animal.create!(name: 'cat')

    assert_selector "h1", text: "HomePage"
  end
end
