class HomePageController < ApplicationController
  def index
  end

  def touch
    Post.transaction do
      sleep 1
      Post.last&.touch
    end

    head :ok
  end
end
